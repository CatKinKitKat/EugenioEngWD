#!/bin/bash

if [ -f ~/tg1/words_dic/words.txt ]; 
then
    rm ~/tg1/words_dic/words.txt
fi

sed -e 's/[^[:alpha:]]/ /g' ~/tg1/corpus_txt/all | tr '\n' " " |  tr -s " " | tr " " '\n'| tr 'A-Z' 'a-z' | sort -d | uniq -c | nl | gawk '{print $3 " " $2}' >> ~/tg1/words_dic/words.txt
echo 'done, check tg1/words_dic'
