#!/bin/bash

if [ -f ~/tg1/corpus_info/corpus_info.txt ]; 
then
    rm ~/tg1/corpus_info/corpus_info.txt
fi

word=$(cat ~/tg1/corpus_txt/all | wc -w | gawk '{print $1}')
wordDiff=$(cat ~/tg1/corpus_txt/all | uniq -u | wc -w | gawk '{print $1}')
wordDiffRatio=$(expr $word - $wordDiff)
sentence=$(cat ~/tg1/corpus_txt/all | wc -l | gawk '{print $1}')
sentenceDiff=$(cat ~/tg1/corpus_txt/all | uniq -u | wc -l | gawk '{print $1}')
sentenceDiffRatio=$(expr $sentence - $sentenceDiff)
chars=$(cat ~/tg1/corpus_txt/all | wc -m | gawk '{print $1}')
charsDiff=$(cat ~/tg1/corpus_txt/all | uniq -u | wc -m | gawk '{print $1}')
charsDiffRatio=$(expr $chars - $charsDiff)

echo 'no palavras: ' >> ~/tg1/corpus_info/corpus_info.txt
echo $word >> ~/tg1/corpus_info/corpus_info.txt
echo 'no palavras diff: ' >> ~/tg1/corpus_info/corpus_info.txt
echo $wordDiff >> ~/tg1/corpus_info/corpus_info.txt
echo 'no palavras diff diferenca: ' >> ~/tg1/corpus_info/corpus_info.txt
echo $wordDiffRatio >> ~/tg1/corpus_info/corpus_info.txt

echo 'no frases: ' >> ~/tg1/corpus_info/corpus_info.txt
echo $sentence >> ~/tg1/corpus_info/corpus_info.txt
echo 'no frases diff: ' >> ~/tg1/corpus_info/corpus_info.txt
echo $sentenceDiff >> ~/tg1/corpus_info/corpus_info.txt
echo 'no frases diff diferenca: ' >> ~/tg1/corpus_info/corpus_info.txt
echo $sentenceDiffRatio >> ~/tg1/corpus_info/corpus_info.txt

echo 'no letras: ' >> ~/tg1/corpus_info/corpus_info.txt
echo $chars >> ~/tg1/corpus_info/corpus_info.txt
echo 'no letras diff: ' >> ~/tg1/corpus_info/corpus_info.txt
echo $charsDiff >> ~/tg1/corpus_info/corpus_info.txt
echo 'no letras diff diferenca: ' >> ~/tg1/corpus_info/corpus_info.txt
echo $charsDiffRatio >> ~/tg1/corpus_info/corpus_info.txt
