#!/bin/bash

if [ -f ~/tg1/words_dic/words_pairs.txt ]; 
then
    rm ~/tg1/words_dic/words_pairs.txt
fi

sed -e 's/[^[:alpha:]]/ /g' ~/tg1/corpus_txt/all |  tr -s " " | tr 'A-Z' 'a-z' | tr " " ", " | gawk -F, '
{
    for(i = 1; i < NF; i++) {
        j = i + 1;
        a[$i,$j] = $i " " $j;
        print a[$i, $j];
    }
}' | tr " " "#" | sort -d | uniq -c | tr "#" " " | gawk '
{
    if ($1 > 3) print $2 " " $3 " " $1;

}' >> ~/tg1/words_dic/words_pairs.txt 

#conideremos que < 3 ocorrencias sao coisas insignificaveis

echo 'done, check tg1/words_dic'
