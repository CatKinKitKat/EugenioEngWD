ECHO OFF

ECHO EugenioEngWD

setlocal enableDelayedExpansion

set OriginalDir=%CD%

set EugenioPath="C:\Program Files\Eug�nio"
set WordsPath="B:\Users\gonca\Downloads\EugenioEngWD\words_dic"
set SentencesPath="B:\Users\gonca\Downloads\EugenioEngWD\sentences_dic"

robocopy %WordsPath% %EugenioPath%
robocopy %SentencesPath% %EugenioPath%

cd %EugenioPath%

rename words.txt geral.pal
rename words_pairs.txt geral.par
rename sentences.txt geral.frs
rename sentences_pairs.txt geral.paf

ECHO TUDO COPIADO COM SUCESSO

PAUSE