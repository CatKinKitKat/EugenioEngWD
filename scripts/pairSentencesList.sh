#!/bin/bash

if [ -f ~/tg1/sentences_dic/sentences_pairs.txt ]; 
then
    rm ~/tg1/sentences_dic/sentences_pairs.txt
fi

sed -e 's/[^[:alpha:]]/ /g' ~/tg1/corpus_txt/all | tr -s " " | tr 'A-Z' 'a-z' | tr " " "|" | tr "\n" " " | gawk '
{
    for(i = 0; i < NF; i++) {
        j = i + 1;
        print $i " " $j;
    }
}' | sort -d | uniq -c | tr -s " " | gawk '{

	if ($1 > 3) {
		for(i = 2; i < NF; i++) {
			j = i + 1;
        		printf $i " " $j " ";
		}
		print $1;
	}

}' >> ~/tg1/sentences_dic/sentences_pairs.txt 

echo 'done, check tg1/sentences_dic'
