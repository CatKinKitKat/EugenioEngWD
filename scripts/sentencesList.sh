#!/bin/bash

if [ -f ~/tg1/sentences_dic/sentences.txt ]; 
then
    rm ~/tg1/sentences_dic/sentences.txt
fi

sed -e 's/[^[:alpha:]]/ /g' ~/tg1/corpus_txt/all | tr -s " " | tr 'A-Z' 'a-z' | tr " " "|" | sort -d | uniq -c | tr -s " " | gawk '
{
    if ($1 > 3) {
	printf $2;
	for (i = 3; i < NF; i++) {
	    print "|" $i;
	}
	print " " $1;
    }
}' >> ~/tg1/sentences_dic/sentences.txt #ignoro todo o que tem < 3 ocorrencias
echo 'done, check tg1/sentences_dic'
