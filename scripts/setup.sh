#!/bin/bash

if [ -x "$(command -v unzip)" ];
then
	cp ~/Downloads/switchboard.zip ~/tg1/corpus/
	unzip ~/tg1/corpus/switchboard.zip
	mv ~/switchboard/* ~/tg1/corpus/
	rm -fr ~/switchboard
	rm ~/tg1/corpus/switchboard.zip

	find ~/tg1/corpus/ -name "*.txt" -exec cat {} >> ~/tg1/corpus_txt/temp \;
	sed -e 's/[ \t]*//' ~/tg1/corpus_txt/temp >> ~/tg1/corpus_txt/temp2
	rm ~/tg1/corpus_txt/temp
	sed -r '/^\s*$/d' ~/tg1/corpus_txt/temp2 >> ~/tg1/corpus_txt/all
	rm ~/tg1/corpus_txt/temp2

	find tg1 -name "*.xml" -exec rm {} \;
	find tg1 -name "*.anc" -exec rm {} \;

else
	echo "Please install unzip"
fi
